<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $guarded=[];

    public function class()
    {
        return $this->hasOne(Classe::Class, 'id', 'class_id');
    }
}
