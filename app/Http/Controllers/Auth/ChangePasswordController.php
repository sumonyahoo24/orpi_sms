<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\support\Facades\Hash;
use Illuminate\support\Facades\Auth;

class ChangePasswordController extends Controller
{
    public function index()
    {
    	return view('auth\passwords\change');
    }

    public function update(Request $request)
    {
    	$this->validate($request,[

    		'oldPassword'=>'required',
    		'password'=>'required|confirmed',
    	]);

    	$hashedPassword=Auth::user()->password;

    	if (Hash::check($request->oldPassword, $hashedPassword)) {
    		$user=User::find(Auth::id());
    		$user->password = Hash::make($request->password);
    		$user->save();
    		Auth::logout();
    		return redirect()->route('login')->with('successMsg', "Password is Successfully changed!");
    	}

    	else{

    		return redirect()->back()->with('errorMsg', "Old password is not correct!");
    	}
    }
}
