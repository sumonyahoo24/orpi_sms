<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Attendance;
use App\Models\Classe;
use App\Models\Classteacher;
use App\Models\Course;
use App\Models\Formmaster;
use App\Models\Mark;
use App\Models\payment;
use App\Models\Salary;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TeacherController extends Controller
{
    public function teacherProfile()
    {
        $id=auth()->user()->id;
        $data=Teacher::where('id', $id)->first();
        return view('frontend.teacher.teacherProfile', compact('data'));
    }
    public function teacherLogin()
    {
        return view('frontend.teacher.teacherLogin');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function teacherLoginProcess(Request $request)
    {
        $credentials = $request->only('email', 'password');
        //dd(Auth::guard('admin')->attempt($credentials));
        if (Auth::guard('web')->attempt($credentials)) {
            // Authentication passed...
            return view('frontend.teacher.teacherIndex');
        }
        elseif (Auth::guard('admin')->attempt($credentials)) {
            // Authentication passed...
            return redirect()->route('adminIndex');
        }
        elseif (Auth::guard('student')->attempt($credentials)) {
            // Authentication passed...
            return redirect()->route('studentInfo');
        }
        return redirect()->back();
    }
    public function takeAttendance()
    {
        $id=auth()->user()->id;
        $checkData=Formmaster::where('teacher_id', $id)->with('class')->first();
        $datas=Student::where('class_id', $checkData->class->id)->orderBy('roll', 'asc')->get();
        $date=Carbon::now()->format('Y-m-d');
        $class=$checkData->class_id;
        $attenCheck=Attendance::where('class_id', $class)->where('date', $date)->get();
        $attenCount=count($attenCheck);
        //dd($attenCount);
        return view('frontend.teacher.takeAttendance', compact('datas', 'attenCount'));
    }
    public function showAttendance()
    {
        $auth_id=auth()->user()->id;
        $class_id=Formmaster::select('class_id')->where('teacher_id', $auth_id)->first();
        $totalStudent=Student::where('class_id', $class_id->class_id)->with('class')->get();
        $totalStudents=count($totalStudent);
        $datas=Attendance::where('date', Carbon::now()->format('Y-m-d'))->where('status', 1)->get();
        $date=Carbon::now()->format('Y-m-d');
        $class=$class_id->class_id;
        //dd($date);
        $attenCheck=Attendance::where('class_id', $class)->where('date', $date)->get();
        $attenCount=count($attenCheck);

        //dd($attenCount);

        $totalPresent=count($datas);
        //dd($totalPresent);
        //if (!empty($datas)){
        //    $task_array = json_decode(($datas->status),true);
        //    $totalPresent=0;
        //    foreach ($task_array as $value)
        //    {
        //        if ($value==1)
        //        {
        //            $totalPresent=$totalPresent+1;
        //        }
        //    }
        //    //dd($total);
        //    return view('frontend.teacher.showAttendance', compact('totalStudents','totalPresent', 'datas'));
        //}
        //dd($total);
        return view('frontend.teacher.showAttendance', compact('totalStudents', 'datas', 'totalPresent', 'attenCount'));

    }
    public function takeAttendanceProcess(Request $request)
    {
        //dd($request->all());
        $auth_id=auth()->user()->id;
        $class_id=Formmaster::select('class_id')->where('teacher_id', $auth_id)->first();
        $date=Carbon::now()->format('Y-m-d');
        //$test=json_encode($request->input('id'));
        $test=$request->input('id');
        $count=count($test);
        //dd($count);
        //$attendance=json_encode($request->input('attendance'));
        $attendance=$request->input('attendance');
        for ($i=0; $i<$count; $i++){
            $bobo=Attendance::create([
                'student_id'=> $test[$i],
                'class_id'=> $class_id->class_id,
                'status'=> $attendance[$i],
                'date'=> $date,
            ]);
            //dd($bobo);
        }
        return redirect()->route('showAttendance');
    }
    public function teacherClassList()
    {
        $auth_id=auth()->user()->id;
        $datas=Classteacher::where('teacher_id', $auth_id)->with('class')->with('course')->get();
        //dd($datas);
        return view('frontend.teacher.teacherClassList', compact('datas'));
    }
    public function selectClass()
    {
        $date=Carbon::now()->format('Y');
        $user_id=auth()->user()->id;
        $datas=Classteacher::with('class')->with('teacher')->where('teacher_id', $user_id)->get();
        $marks=Mark::where('year', $date)->with('class')->with('student')->orderBy('class_id', 'asc')->get();
        //dd($marks);
        return view('frontend.teacher.selectClass', compact('datas', 'marks'));
    }
    public function selectClassProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_id' => 'required',
        ]);
        $class_id=$request->input('class_id');
        $user_id=auth()->user()->id;
        $datas=Classteacher::with('course')->where('class_id', $class_id)->where('teacher_id', $user_id)->get();
        //dd($datas);
        $students=Student::where('class_id', $class_id)->get();
        //dd($students);
        return view('frontend.teacher.selectCourse', compact('datas', 'students', 'class_id'));
    }
    public function jsonAddMarks(Request $request)
    {
        $data=Mark::select('first_term','mid_term', 'final_term')->where('student_id',$request->id)->take(100)->first();
        return response()->json($data);
    }
    public function submitmarks(Request $request)
    {
        $validatedData = $request->validate([
            'class_id' => 'required',
            'subject_id' => 'required',
        ]);
        //dd($request->all());
        $class_id=$request->input('class_id');
        $subject_id=$request->input('subject_id');
        $student_id=$request->input('student_id');
        if($request->input('first_term')=="undefined" || $request->input('first_term')=="null"){
            $first_term=null;
        }else{
            $first_term=$request->input('first_term');
        }
        if($request->input('mid_term')=="undefined" || $request->input('mid_term')=="null"){
            $mid_term=null;
        }else{
            $mid_term=$request->input('mid_term');
        }
        if($request->input('final_term')=="undefined" || $request->input('final_term')=="null"){
            $final_term=null;
        }else{
            $final_term=$request->input('final_term');
        }
        $year=Carbon::now()->format('Y');
        $findMarks=Mark::where('class_id', $class_id)->where('subject_id', $subject_id)->where('student_id', $student_id)->where('year', $year)->first();
        //dd($findMarks);
        if(empty($findMarks)){
            $test=Mark::create([
                'class_id'=> $class_id,
                'subject_id'=> $subject_id,
                'student_id'=> $student_id,
                'first_term'=> $first_term,
                'mid_term'=> $mid_term,
                'final_term'=> $final_term,
                'year'=> $year,
                'status'=> "pending",
            ]);
        }else{
            $test=Mark::where('id', $findMarks->id)->update([
                'class_id'=> $class_id,
                'subject_id'=> $subject_id,
                'student_id'=> $student_id,
                'first_term'=> $first_term,
                'mid_term'=> $mid_term,
                'final_term'=> $final_term,
                'year'=> $year,
                'status'=> "pending",
            ]);
            //dd($test);
        }
        return redirect()->route('selectClass');
    }
    public function submitMarksList()
    {
        return view('frontend.teacher.submitMarksList');
    }
    public function teacherSalary()
    {
        $date=Carbon::now()->format('m-Y');
        $userId=auth()->user()->id;
        $data=Salary::where('teacher_id', $userId)->where('date', $date)->first();
        return view('frontend.teacher.salary', compact('data'));
    }
}
