<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Attendance;
use App\Models\Classe;
use App\Models\Mark;
use App\Models\payment;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function studentLogin()
    {
        return view('frontend.student.login');
    }
    public function studentLoginProcess(Request $request)
    {
        $credentials = $request->only('email', 'password');
        //dd(Auth::attempt($credentials));
        if (Auth::guard('student')->attempt($credentials)) {
            // Authentication passed...
            $id=Auth::guard('student')->user()->id;
            $info=Student::where('id', $id)->first();
            //dd($info);
            return view('frontend.student.studentIndex', compact('info'));
        }
        return redirect()->back();
    }
    public function studentInfo()
    {
        $id=Auth::guard('student')->user()->id;
        $info=Student::where('id', $id)->first();
        return view('frontend.student.studentIndex', compact('info'));
    }
    public function attendanceInfo()
    {
        $id=Auth::guard('student')->user()->id;
        $atttendance=Attendance::where('student_id', $id)->get();
        $totalPresent=count($atttendance->where('status', 1));
        $totalAttendance=count($atttendance);
        return view('frontend.student.attendanceInfo', compact('totalAttendance', 'totalPresent'));
    }
    public function resultIfo()
    {
        $id=Auth::guard('student')->user()->id;
        $data=Mark::where('student_id', $id)->where('status', "publish")->first();
        if (!empty($data)){

            if($data->mid_term==null && $data->final_term==null){
                $totalMarks=($data->first_term);
            }elseif ($data->first_term==null && $data->final_term==null){
                $totalMarks=($data->mid_term);
            }elseif ($data->first_term==null && $data->mid_term==null){
                $totalMarks=($data->final_term);
            }elseif ($data->first_term==null){
                $totalMarks=((($data->mid_term)+($data->final_term))/2);
            }elseif ($data->mid_term==null){
                $totalMarks=((($data->first_term)+($data->final_term))/2);
            }elseif ($data->final_term==null){
                $totalMarks=((($data->first_term)+($data->mid_term))/2);
            }
            else{
                $totalMarks=((($data->first_term)+($data->mid_term)+($data->final_term))/3);
            }
        }

        //dd($totalMarks);
        return view('frontend.student.resultInfo', compact('data', 'totalMarks'));
    }
    public function paymentInfo()
    {
        $userId=Auth::guard('student')->user()->id;
        $data=payment::with('total')->where('student_id', $userId)->get();
        //dd($data);
        return view('frontend.student.paymentInfo ', compact('data'));
    }
}
