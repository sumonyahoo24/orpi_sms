@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                    @if (session('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            {{ session('message')}}
                        </div>
                    @endif
                    <h4 class="card-title">Payments Lists</h4>
                    <a href="{{route('createPayments')}}">
                        <button type="button" class="btn btn-success btn-block" >Add Payments
                            <i class="mdi mdi-plus"></i>
                        </button>
                    </a>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Fee Type
                                </th>
                                <th>
                                    Total Amount
                                </th>
                                <th>
                                    Description
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            ?>
                            @foreach($datas as $data)
                                <tr>
                                    <td class="font-weight-medium">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        {{$data->student->first_name}} {{$data->student->last_name}}
                                    </td>
                                    <td>
                                        {{$data->total['amount']}}
                                    </td>
                                    <td>
                                        {{$data->description }}
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="{{route('paymentDelete', $data->id)}}">
                                            <span class="icon remove_icon">
                                                <i class="fa fa-trash"></i>
                                            </span>
                                        </a>
                                        <a href="{{route('paymentPrint', $data->id)}}">
                                            <span class="icon print">
                                                <i class="fa fa-print"></i>
                                            </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
