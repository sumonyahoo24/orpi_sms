@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Create Payments</h4>
                    <form action="{{route('createPaymentProcess')}}" method="post">
                        @CSRF
                        <div class="form-group">
                            <label for="student_id">Student ID:</label>
                            <select class="form-control" name="student_id" id="student_id">
                                @foreach($student as $data)
                                    <option value="{{$data->id}}">{{$data->first_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        {{--<div class="form-group row">--}}
                            {{--<label for="student_id" class="col-sm-3 col-form-label">Student ID</label>--}}
                            {{--<div class="col-sm-9">--}}
                                {{--$student<input type="text" class="form-control" id="student_id" name="student_id" placeholder="Fees Type">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group d-flex pay_select">
                            <label for="sel1">Select Payment:</label>
                            <select class="form-control" id="paymentType" name="total_amount">
                                <option value="">----Select Payment Type----</option>
                            @foreach($datas as $data)
                                    <option value="{{$data->id}}">{{$data->type}}</option>
                            @endforeach
                            </select>
                            <div class="amount" id="paymentValue">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="5" id="description" name="description"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                        {{--<div class="form-group">--}}
                            {{--<table class="table table-bordered">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Values</th>--}}

                                    {{--<th><a href="#" class="addRow"><i class="glyphicon glyphicon-plus"></i></a></th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                    {{--<td><label for="values">Values</label></td>--}}
                                    {{--<td><input id="values" type="text" name="values[]" class="form-control" required=""/></td>--}}
                                    {{--<td><a href="#" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}

                                {{--</tbody>--}}
                                {{--<tfoot>--}}
                                {{--<tr>--}}
                                    {{--<td style="border: none"></td>--}}
                                    {{--<td style="border: none"></td>--}}
                                    {{--<td style="border: none"></td>--}}

                                    {{--<td><input type="submit" name="" value="Add" class="btn btn-success"></td>--}}
                                {{--</tr>--}}
                                {{--</tfoot>--}}
                            {{--</table>--}}
                        {{--</div>--}}
                </div>




                @endsection

                @push('scripts')

                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                            crossorigin="anonymous"></script>



                    <script type="text/javascript">

                        $('.addRow').on('click',function(){
                            addRow();
                        });
                        function addRow()
                        {
                            var tr=' <div class="form-group d-flex pay_select">' +
            '                            <label for="sel1">Select list:</label>' +
            '                            <select class="form-control" id="paymentType">' +
            '                               @foreach($datas as $data)' +
            '                                    <option value="{{$data->id}}">{{$data->type}}</option>' +
            '                               @endforeach' +
            '                            </select>' +
            '                            <div class="amount" id="paymentValue">' +
            '                            </div>' +
            '                        </div>';
                            $('tbody').append(tr);

                        };

                        $('.remove').live('click',function(){

                            var last=$('tbody tr').length;
                            if(last==1){
                                alert("no");
                            }
                            else
                            {

                                $(this).parent().parent().remove();
                            }

                        });




                    </script>

                    <script src="{{asset('tag/bootstrap-tagsinput.js')}}"></script>
                    <script src="{{asset('tag/tagsinput.js')}}"></script>

    @endpush

