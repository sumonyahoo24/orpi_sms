@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @if(Session::has('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4 class="card-title">Salary Lists</h4>
                    <a href="{{route('createSalary')}}">
                        <button type="button" class="btn btn-success btn-block" >Add Salary
                            <i class="mdi mdi-plus"></i>
                        </button>
                    </a>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Teacher Name
                                </th>
                                <th>
                                    Total Amount
                                </th>
                                <th>
                                    Date
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            ?>
                            @foreach($datas as $data)
                                <tr>
                                    <td class="font-weight-medium">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        {{$data->teacher_id}}
                                    </td>
                                    <td>
                                        {{$data->amount }}
                                    </td>
                                    <td>
                                        {{$data->date }}
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="{{route('editSalary', $data->id)}}">
                                            <span class="icon edit_icon">
                                                <i class="fa fa-edit"></i>
                                            </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
