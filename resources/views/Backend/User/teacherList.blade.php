@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @if(Session::has('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4 class="card-title">Teacher Lists</h4>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success btn-block" data-toggle="modal"
                            data-target="#exampleModal">Add Teacher
                        <i class="mdi mdi-plus"></i>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog_lg" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Teacher</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('createTeacher')}}" method="post">
                                        @CSRF
                                        <div class="form-group row">
                                            <label for="first_name" class="col-sm-2 col-form-label">First Name</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="first_name"
                                                       name="first_name" placeholder="First Name">
                                            </div>
                                            <label for="last_name" class="col-sm-2 col-form-label">Last Name</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="last_name" name="last_name"
                                                       placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group d-flex pay_select">
                                            <label for="gender">Select Gender:</label>
                                            <select class="form-control" name="gender" id="gender">
                                                <option value="">Select Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="others">Others</option>
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                            <label for="present_address" class="col-sm-2 col-form-label">Present
                                                Address</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="present_address"
                                                       name="present_address" placeholder="Present Address">
                                            </div>
                                            <label for="password" class="col-sm-2 col-form-label">Password</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="password" name="password"
                                                       placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-4">
                                                <input type="email" class="form-control" id="email" name="email"
                                                       placeholder="Email">
                                            </div>
                                            <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                            <div class="col-sm-4">
                                                <input type="number" class="form-control" id="phone" name="phone"
                                                       placeholder="Phone">
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Phone
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Gender
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($datas as $data)
                                <tr>
                                    <td class="font-weight-medium">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        {{$data->first_name}} {{$data->last_name}}
                                    </td>
                                    <td>
                                        {{$data->phone}}
                                    </td>
                                    <td>
                                        {{$data->email}}
                                    </td>
                                    <td>
                                        {{$data->gender}}
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="{{route('editTeacher', $data->id)}}">
                                    <span class="icon edit_icon">
                                        <i class="fa fa-edit"></i>
                                    </span>
                                        </a>

                                        <a href="{{route('teacherDelete',$data->id )}}">

                                        <span class="icon remove_icon">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
