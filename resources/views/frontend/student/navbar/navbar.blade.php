<div class="row w-100 pt-lg-5">
    <div class="col-lg-12 mx-auto teacher_btn">
        <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-info">
                <a href="{{route('studentInfo')}}">Profile</a>
            </button>
            <button type="button" class="btn btn-success">
                <a href="{{route('attendanceInfo')}}">Show Attendance</a>
            </button>
            <button type="button" class="btn btn-dark">
                <a href="{{route('paymentInfo')}}">Payment History</a>
            </button>
            <button type="button" class="btn btn-success">
                <a href="{{route('resultIfo')}}">Result</a>
            </button>
        </div>
    </div>
</div>
@foreach ($errors->all() as $error)
    <p class="alert alert-danger">{{ $error }}</p>
@endforeach
@if (session('message'))
    <div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
        {{ session('message')}}
    </div>
@endif
