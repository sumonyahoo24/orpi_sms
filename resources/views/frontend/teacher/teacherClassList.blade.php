@extends('frontend.master')
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-1 theme-one">
                @include('frontend.teacher.navbar.navbar')
                <div class="list-group col-md-6 col_center">
                    <a href="#" class="list-group-item list-group-item-action active">
                        CLASS LIST
                    </a>
                    @foreach($datas as $data)
                    <a href="#" class="list-group-item menu_icon list-group-item-action">
                        <i class="fa fa-arrow-circle-right"></i> {{$data->class->name}} <span class="badge badge-pill badge-primary pull-right">{{$data->course->name}}</span>
                    </a>
                        @endforeach
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
