@extends('frontend.master')
@section('content')
    <div class="container-scroller tec_sec_full pt-5">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-2 theme-one">
                @include('frontend.teacher.navbar.navbar')
                <div class="row d-flex w-100">
                    <div class="col-lg-8 mx-auto teacher_btn">
                        <section class="tec_sec_info">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1>Teacher Information</h1>
                                        <div class="row">
                                            <div class="col-md-6 mx-auto">
                                                <div class="tec_info">
                                                    <span class="title">Name:</span>
                                                    <span class="title_data">{{$data->first_name}}</span>
                                                </div>
                                                <div class="tec_info">
                                                    <span class="title">Phone:</span>
                                                    <span class="title_data">{{$data->phone}}</span>
                                                </div>
                                                <div class="tec_info">
                                                    <span class="title">Email:</span>
                                                    <span class="title_data">{{$data->email}}</span>
                                                </div>
                                                <div class="tec_info">
                                                    <span class="title">Address:</span>
                                                    <span class="title_data">{{$data->present_address}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
