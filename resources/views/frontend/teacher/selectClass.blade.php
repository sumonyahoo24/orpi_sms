@extends('frontend.master')
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-1 theme-one">
                @include('frontend.teacher.navbar.navbar')
                <form action="{{route('selectClassProcess')}}" method="post" role="form">
                    @csrf
                    <div class="col-md-6 col_center select_class_all">
                        <div class="form-group">
                            <label for="class_id">Select Class</label>
                            <select class="form-control" name="class_id" id="class_id">
                                <option value="0">Select Class Name</option>
                                @foreach($datas as $data)
                                    <option value="{{$data->id}}">{{$data->class->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-lg btn-success float-right">Submit</button>
                    </div>
                </form>
                <div class="row">
                    <div class="col-lg-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Mark's of the Students</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>
                                                #
                                            </th>
                                            <th>
                                                Class Name
                                            </th>
                                            <th>
                                                Student Name
                                            </th>
                                            <th>
                                                First Term
                                            </th>
                                            <th>
                                                Mid Term
                                            </th>
                                            <th>
                                                Final
                                            </th>
                                            <th>
                                                Grade
                                            </th>
                                            <th>
                                                Year
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i=1;
                                        ?>
                                        @foreach($marks as $data)
                                            <tr>
                                                <td class="font-weight-medium">
                                                    {{$i++}}
                                                </td>
                                                <td>
                                                    {{$data->class['name']}}
                                                </td>
                                                <td>
                                                    {{$data->student['first_name']}}  {{$data->student['last_name']}}
                                                </td>
                                                <td>
                                                    {{$data->first_term}}
                                                </td>
                                                <td>
                                                    {{$data->mid_term}}
                                                </td>
                                                <td>
                                                    {{$data->final_term}}
                                                </td>
                                                <td>
                                                    <?php
                                                        if($data->mid_term==null && $data->final_term==null){
                                                            $totalMarks=($data->first_term);
                                                        }elseif ($data->first_term==null && $data->final_term==null){
                                                            $totalMarks=($data->mid_term);
                                                        }elseif ($data->first_term==null && $data->mid_term==null){
                                                            $totalMarks=($data->final_term);
                                                        }elseif ($data->first_term==null){
                                                            $totalMarks=((($data->mid_term)+($data->final_term))/2);
                                                        }elseif ($data->mid_term==null){
                                                            $totalMarks=((($data->first_term)+($data->final_term))/2);
                                                        }elseif ($data->final_term==null){
                                                            $totalMarks=((($data->first_term)+($data->mid_term))/2);
                                                        }
                                                        else{
                                                            $totalMarks=((($data->first_term)+($data->mid_term)+($data->final_term))/3);
                                                        }
                                                    ?>
                                                    @switch($totalMarks)
                                                        @case($totalMarks>=80)
                                                        A+
                                                        @break
                                                        @case($totalMarks>=70)
                                                        A
                                                        @break
                                                        @case($totalMarks>=60)
                                                        A-
                                                        @break
                                                        @case($totalMarks>=50)
                                                        B
                                                        @break
                                                        @case($totalMarks>=40)
                                                        C
                                                        @break
                                                        @default
                                                        F
                                                        @break
                                                    @endswitch
                                                </td>
                                                <td>
                                                    {{$data->year}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
