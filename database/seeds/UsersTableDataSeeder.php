<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

        public function run()
    {

        DB::table('users')->insert([
            "first_name" => "Admin",
            "last_name" => "admin",
            'present_address' => 'Kawlar,bazar',
            "phone" => "01770146145",
            "email" => "admin@gmail.com",
            "password" => bcrypt('123456'),
            "amount" => "10000",

        ]);
    }

}
